using System;
using RedSunsetCore.Services.Sounds;
using RedSunsetCore.Services.Sounds.Interfaces;
using UnityEngine;
using Zenject;

public class TestSounds : MonoBehaviour
{
	[SerializeField]
	private SoundDefinition music;
	
	[SerializeField]
	private SoundDefinition fx;

	[Inject]
	private ISoundService mSoundService;

	private void Start()
	{
		mSoundService.PeekAudioPlayer(music).Play(-1);
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			mSoundService.PeekAudioPlayer(fx).PlayXShots(1);
		}else if (Input.GetMouseButtonDown(1))
		{
			mSoundService.PeekAudioPlayer(fx).PlayXShots(10);
		}
	}
}
