using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.SlidePuzzle.Events;
using RedSunsetCore.Services.Event.Interfaces;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RedSunsetCore.GameTools.SlidePuzzle.Tools
{
    public class CameraDeviceSelector : MonoBehaviour
    {
        [SerializeField] private Button takePhotoBtn;

        [SerializeField] private Dropdown availableDevicesDropdown;

        [SerializeField] private RawImage targetImageContainer;

        [Inject] private IEventService eventService;

        private RectTransform targetImageContainerRectTransform;
        private WebCamTexture webCamTexture;

        private void Start()
        {
            targetImageContainerRectTransform = targetImageContainer.GetComponent<RectTransform>();
            StartCoroutine(GetPermissionsAndDevices());
        }

        private IEnumerator GetPermissionsAndDevices()
        {
            yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
            if (Application.HasUserAuthorization(UserAuthorization.WebCam))
            {
                var devices = new List<Dropdown.OptionData>();
                foreach (WebCamDevice webCamDevice in WebCamTexture.devices)
                    devices.Add(new Dropdown.OptionData(webCamDevice.name));
                availableDevicesDropdown.options = devices;
                availableDevicesDropdown.onValueChanged.AddListener(OnChangeSourceDevice);
                OnChangeSourceDevice(0);

                takePhotoBtn.onClick.AddListener(OnTakePhoto);
            }
        }

        private void OnTakePhoto()
        {
            takePhotoBtn.onClick.RemoveListener(OnTakePhoto);
            availableDevicesDropdown.onValueChanged.RemoveListener(OnChangeSourceDevice);
            takePhotoBtn.gameObject.SetActive(false);

            Color32[] pixels = webCamTexture.GetPixels32();
            var photo = new Texture2D(webCamTexture.width, webCamTexture.height);
            photo.SetPixels32(pixels);
            photo.Apply();
            targetImageContainer.texture = photo;
            webCamTexture.Stop();

            eventService.Dispatch<StartGameEvent>(ev => { ev.TargetTexture = photo; });
        }

        private void OnChangeSourceDevice(int index)
        {
            Vector2 imageSize = targetImageContainerRectTransform.sizeDelta;
            webCamTexture = new WebCamTexture(WebCamTexture.devices[index].name, (int)imageSize.x, (int)imageSize.y);
            targetImageContainer.texture = webCamTexture;
            webCamTexture.Play();
        }
    }
}