using System;
using System.Collections;
using RedSunsetCore.AI;
using UnityEngine;

namespace AITest
{
	public class MarkovTest : MonoBehaviour
	{
		[SerializeField]
		private float percentageTrained;
		[SerializeField]
		private string userInput;
		[SerializeField]
		private int responseLength;

		private MarkovChain ai;

		private void Start()
		{
			percentageTrained = 0f;
			ai = new MarkovChain();
			var trainingData = Resources.Load<TextAsset>("AITrainingData");
			StartCoroutine(TrainRoutine(trainingData.text));
		}

		private IEnumerator TrainRoutine(string input)
		{
			string[] separatedInput = input.Split('.');
			var currentProcessed = 0;
			int totalSentences = separatedInput.Length;
			foreach (string sentence in separatedInput)
			{
				ai.Train(sentence + ".");
				currentProcessed++;
				percentageTrained = currentProcessed * 100f / totalSentences;
				yield return null;
			}
			
			print($"RESPONSE: {ai.GenerateResponse(userInput, responseLength)}");
			print($"DATA: {ai.GetJsonData()}");
		}
	}
}