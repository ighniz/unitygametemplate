using RedSunsetCore.Config;
using RedSunsetCore.Services.Config;
using UnityEngine;
using Zenject;

public class TestGameConfig : MonoBehaviour
{
    [Inject]
    private IConfigService coreFrameworkConfig;
    
    void Start()
    {
        print(coreFrameworkConfig.GetConfig<StringPoolConfig>());
    }
}
