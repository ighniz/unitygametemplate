using RedSunsetCore.ExtensionMethods;
using RedSunsetCore.GameTools.Generators.Levels;
using UnityEngine;

public class TestCreateLevel : MonoBehaviour
{
    [SerializeField]
    private LevelGeneratorConfig levelGeneratorConfig;
    
    private void Start()
    {
        LevelData levelData = LevelGenerator.Create(levelGeneratorConfig)
            .GenerateLevelData();

        print(levelData.ToJson(true));
    }
}
