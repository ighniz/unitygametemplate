using System;
using RedSunsetCore.Services.Tutorial.Interfaces;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Tutorials
{
    public class TutorialTrigger : MonoBehaviour
    {
        [Inject]
        private ITutorialService mTutorialService;

        private void Start()
        {
            mTutorialService.Start("Tutorial Example");
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(1))
            {
                mTutorialService.GoToStep(Random.Range(0, 5));
            }
        }
    }
}
