using System;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using RedSunsetCore.GameTools.Generators.Dungeons.ExtensionMethods;
using UnityEditor;
using UnityEngine;

namespace Dungeons
{
	public class DungeonTileTest : MonoBehaviour
	{
		public DungeonPointSide side;
		public List<Vector2Int> neighbors = new ();
		public DungeonPointType pointType;

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.white;
			Vector2Int[] directionVectors = side.ToDirectionVectors();
			Vector3 cachedPosition = transform.position;
			foreach (Vector2Int direction in directionVectors)
			{
				var castedDirection = new Vector3(direction.x, 0, direction.y);
				Handles.ArrowHandleCap(0, cachedPosition, Quaternion.LookRotation(castedDirection), 1f, EventType.Repaint);
			}
		}
	}
}