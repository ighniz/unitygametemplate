using System;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using UnityEngine;

namespace Dungeons
{
	[Serializable]
	public class DictionarySideTypeGameObject : SerializableDictionary<DungeonPointSide, GameObject>
	{
		
	}
}