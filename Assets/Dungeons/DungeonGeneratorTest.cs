using System;
using System.Collections;
using System.Collections.Generic;
using RedSunsetCore.GameTools.Generators.Dungeons;
using RedSunsetCore.GameTools.Generators.Dungeons.Config;
using RedSunsetCore.GameTools.Generators.Dungeons.Data;
using RedSunsetCore.GameTools.Generators.Dungeons.Enums;
using RedSunsetCore.GameTools.Generators.Dungeons.Interfaces;
using UnityEngine;

namespace Dungeons
{
	public class DungeonGeneratorTest : MonoBehaviour
	{
		[SerializeField]
		private GameObject wallPrefab;
		[SerializeField]
		private GameObject roomPrefab;
		[SerializeField]
		private GameObject corridorPrefab;
		[SerializeField]
		private GameObject deadEndPrefab;
		[SerializeField]
		private GameObject undefinedPrefab;
		[SerializeField]
		private GameObject entrancePrefab;
		[SerializeField]
		private GameObject itemPrefab;
		[SerializeField]
		private DungeonGenerationConfig config;
		[SerializeField]
		private GameObject character;
		[SerializeField]
		private Transform characterCameraRoot;

		[SerializeField]
		private DictionarySideTypeGameObject tilesType;

		private Dictionary<DungeonPointType, GameObject> viewMap = new();
		private Dictionary<DungeonTile, GameObject> mAllPointsGO = new();
		private DungeonGeneratorFactory mDungeonGenerator;

		private IEnumerator Start()
		{
			viewMap.Add(DungeonPointType.Corridor, corridorPrefab);
			viewMap.Add(DungeonPointType.Wall, wallPrefab);
			viewMap.Add(DungeonPointType.Room, roomPrefab);
			viewMap.Add(DungeonPointType.DeadEnd, deadEndPrefab);
			viewMap.Add(DungeonPointType.Undefined, undefinedPrefab);
			viewMap.Add(DungeonPointType.Entrance, entrancePrefab);

			mDungeonGenerator = new DungeonGeneratorFactory();
			mDungeonGenerator.WithConfig(config).GenerateCorridorsFirst();

			InstantiateTiles(mDungeonGenerator.AllRooms);
			InstantiateTiles(mDungeonGenerator.AllCorridors);
			InstantiateTiles(mDungeonGenerator.AllEntrances);

			yield return null;
			
			character.SetActive(true);
			transform.SetParent(characterCameraRoot);
			transform.localPosition = new Vector3(0, 0.701999903f, -4.04699945f);
		}

		private void InstantiateTiles<TEnvironment>(Dictionary<Guid, TEnvironment> environmentData) where TEnvironment : IDungeonEnvironment
		{
			foreach ((Guid key, TEnvironment environment) in environmentData)
			{
				float hue = Mathf.InverseLerp(0, mDungeonGenerator.BiggerRoom.TileCount, environment.TileCount);
				var color = Color.HSVToRGB(hue, 1, 1);
				var roomName = $"{typeof(TEnvironment).Name} : {key}";
				GameObject parentGO = GameObject.Find(roomName) ?? new GameObject(roomName);

				foreach (DungeonTile tile in environment)
				{
					GameObject targetPrefab = GetPrefabForTile(tile);

					//Assert.IsTrue(mAllPointsGO.ContainsKey(tile), $"The tile {tile.Position} already exist!");
					if (!mAllPointsGO.ContainsKey(tile))
					{
						GameObject go = Instantiate(
							targetPrefab, 
							new Vector3(tile.Position.x, 0, tile.Position.y),
							Quaternion.identity);
						mAllPointsGO.Add(tile, go);

						var testComponent = go.AddComponent<DungeonTileTest>();
						testComponent.side = tile.PointSide;
						testComponent.pointType = tile.PointType;
						testComponent.gameObject.name = tile.Position.ToString();
						foreach ((Vector2Int direction, DungeonTile _) in tile.Neighbors)
						{
							testComponent.neighbors.Add(direction);
						}
						
						mAllPointsGO[tile].GetComponentInChildren<MeshRenderer>().material.color = color;
						mAllPointsGO[tile].transform.parent = parentGO.transform;
						
						foreach (DungeonItem item in tile.Items)
						{
							GameObject itemGo = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity);
							itemGo.transform.SetParent(testComponent.transform, false);
						}
					}
				}
			}
		}

		private GameObject GetPrefabForTile(DungeonTile tile)
		{
			GameObject targetPrefab;

			if (tile.PointType == DungeonPointType.Room)
			{
				DungeonPointSide targetSide = GetSideWithoutDiagonals(tile.PointSide);
				if (!tilesType.TryGetValue(targetSide, out targetPrefab))
				{
					targetPrefab = viewMap[DungeonPointType.Undefined];
				}
			}
			else
			{
				targetPrefab = viewMap[tile.PointType];
			}

			return targetPrefab;
		}

		private DungeonPointSide GetSideWithoutDiagonals(DungeonPointSide pointSide)
		{
			return pointSide & ~(DungeonPointSide.BottomLeft | DungeonPointSide.BottomRight | DungeonPointSide.TopLeft | DungeonPointSide.TopRight);
		}
	}
}